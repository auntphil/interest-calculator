import React from 'react'
import PropTypes from 'prop-types';
import { yearMonthCalendar } from '../utils/format-dates';

const MonthView = ({paymentInfo}) => (
    <div id="month_view_wrapper">
        <h3>Monthly Payoff Schedule</h3>
        <ul id="month_view">
            {paymentInfo.map( (month, index) => (
                <li className="month" key={index}>
                    <div className="title">
                        {yearMonthCalendar(index+1)}
                    </div>
                    <ul className="info">
                        <li>${month.balance}</li>
                        <li>Int: ${(month.interest).toFixed(2)}</li>
                        <li>Pay: ${month.payment}</li>
                    </ul>
                </li>
            ))}
    </ul>
  </div>
)

MonthView.propTypes = {
    paymentInfo: PropTypes.array
}

export default MonthView